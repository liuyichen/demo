import Vue from 'vue'
import App from './App'
import navBar from "component/navbar/navbar.vue"
import tabBar from "component/tabbar/tabbar.vue"
import uView from 'uview-ui';
import GoEasy from "./lib/goeasy-2.0.13.min.js";
Vue.config.productionTip = false


App.mpType = 'app'
Vue.component('nav-Bar',navBar)
Vue.component('tab-Bar',tabBar)
Vue.use(uView);
const app = new Vue({
    ...App
})
app.$mount()
const goEasy = GoEasy.getInstance({
   host: "hangzhou.goeasy.io", //应用所在的区域地址: 【hangzhou.goeasy.io |singapore.goeasy.io】
   appkey: "BC-8fab838337e94092b59f9316be5928e8", // common key,
   modules: ['im','pubsub']
});

Vue.prototype.GoEasy = GoEasy;
Vue.prototype.goEasy = goEasy;

Vue.prototype.formatDate = function (t) {
    t = t || Date.now();
    let time = new Date(t);
    let str = time.getMonth() < 9 ? ('0' + (time.getMonth() + 1)) : (time.getMonth() + 1);
    str += '-';
    str += time.getDate() < 10 ? ('0' + time.getDate()) : time.getDate();
    str += ' ';
    str += time.getHours();
    str += ':';
    str += time.getMinutes() < 10 ? ('0' + time.getMinutes()) : time.getMinutes();
    return str;
}
