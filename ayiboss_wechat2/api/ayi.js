import request from "@/utils/request.js";

//开启小程序初始化
export function ayiInit(data) {
	return request.post('/api/common/init', data);
}
//授权登录
export function ayiLogin(data) {
	return request.post('/api/zhiqian.user/login_boss', data);
}
//登陆成功跳转
export function getPageUrl(data) {
	return request.post('/api/zhiqian.user/initBoss', data);
}
//需求列表
export function getJobList(data) {
	return request.get('/api/needs.needs/needs_list', data);
}
//岗位类型
export function workType() {
	return request.get('/api/dict.index/worker');
}
//登陆成功选择岗位
export function initWorker(data) {
	return request.post('/api/zhiqian.user/initWorker', data);
}
//排序规则
export function getSortList() {
	return request.get('/api/needs.needs/index');
}
//退出登录
export function loginOut() {
	return request.get('/api/user/logout');
}
//获取用户认证信息
export function getUserInfo() {
	return request.post('/api/real.index/index');
}
//提交用户认证信息
export function postUserInfo(data) {
	return request.post('/api/real.index/edit', data);
}

//获取用户基本信息
export function getUserBasicInfo() {
	return request.post('/api/worker.basic/index');
}
//提交用户基本信息
export function postUserBasicInfo(data) {
	return request.post('/api/worker.basic/edit', data);
}

//已开通的服务
export function getWorkService() {
	return request.get('/api/worker.service/index');
}
//雇员状态列表
export function getUserStatus() {
	return request.get('/api/dict.index/status');
}
//雇员当前状态
export function getusernewStatus() {
	return request.get('/api/worker.status/index');
}
//修改雇员状态
export function changeUserStatus(data) {
	return request.post('/api/worker.status/edit', data);
}
//学历信息
export function getEdulist() {
	return request.get('/api/dict.index/edu');
}
//授权手机号
export function getMobile(data) {
	return request.post('/api/zhiqian.user/getMobile', data);
}
//获取验证码
export function getMobileCode(data) {
	return request.post('/api/sms/send', data);
}
//检验检验码
export function checkSms(data) {
	return request.post('/api/sms/check', data);
}
//民族
export function getNation() {
	return request.get('/api/dict.others/nation');
}
//星座
export function getSign() {
	return request.get('/api/dict.others/sign');
}
//语言
export function getLanguage() {
	return request.get('/api/dict.others/language');
}
//宗教
export function getFaith() {
	return request.get('/api/dict.others/faith');
}
//个性特点
export function getPersonality() {
	return request.get('/api/dict.index/personality');
}
//获取证件材料列表
export function getCertlist(data) {
	return request.get('/api/worker.cert/index', data);
}
//提交证件材料
export function postCert(data) {
	return request.post('/api/worker.cert/edit', data);
}

//菜系
export function getFooddish() {
	return request.get('/api/dict.food/dish');
}
//主食
export function getFood() {
	return request.get('/api/dict.food/index');
}
//烹饪方式
export function getFoodrecipe() {
	return request.get('/api/dict.food/recipe');
}
//辅食
export function getFoodsupplement() {
	return request.get('/api/dict.food/supplement');
}
//早教
export function getEarly() {
	return request.get('/api/dict.others/early');
}
//获取工作经历
export function getWorklist(data) {
	return request.get('/api/worker.experience/index', data);
}
//添加工作经历
export function addWork(data) {
	return request.post('/api/worker.experience/add', data);
}
//编辑工作经历
export function editWork(data) {
	return request.post('/api/worker.experience/edit', data);
}
//删除工作经历
export function delWork(data) {
	return request.get('/api/worker.experience/del', data);
}
//获取培训经历
export function getTrainlist(data) {
	return request.get('/api/worker.train/index', data);
}
//添加培训经历
export function addTrain(data) {
	return request.post('/api/worker.train/add', data);
}
//编辑培训经历
export function editTrain(data) {
	return request.post('/api/worker.train/edit', data);
}
//删除培训
export function delTrain(data) {
	return request.get('/api/worker.train/del', data);
}
//获取岗位详情
export function getServiceInfo(url) {
	return request.get(url);
}
//保存岗位信息
export function saveService(url, data) {
	return request.post(url, data);
}
//视频邀请
export function getVideolist(data) {
	return request.get('/api/vlink.index/boss_list', data);
}
//接收/忽略视频邀请
export function vlinkConfirm(data) {
	return request.post('/api/vlink.index/confirm', data);
}
//启动视频
export function videoInit(data) {
	return request.post('/api/vlink.index/videoInit', data);
}
//邀请（雇主）
export function subInvite(data) {
	return request.post('/api/vlink.index/add', data);
}
//聊天常用语
export function getLanguageList(data) {
	return request.get('/api/im.index/words', data);
}
//背调订单列表
export function beidiaoOrderList(data) {
	return request.get('/api/beidiao.order/order_list', data);
}
//生成背调订单
export function beidiaoOrder(data) {
	return request.post('/api/beidiao.order/add', data);
}
//背调订单支付
export function beidiaoPay(data) {
	return request.post('/api/pay/pre_pay_beidiao', data);
}
//取消背调
export function beidiaoCancel(data) {
	return request.get('/api/beidiao.order/cancel', data);
}
//获取h5页面

export function getWebview(data) {
	return request.get('/api/baoxian.index/start', data);
}
//隐私协议
export function getRichText(data) {
	return request.get('/addons/shopro/index/richtext', data);
}
//公用get请求
export function publicApiGet(url, data) {
	return request.get(url, data);
}
//公用post请求
export function publicApiPost(url, data) {
	return request.post(url, data);
}
//岗位服务价格列表

export function getMoneyList(data) {
	return request.get('/api/worker.service/money_list', data);
}
//设置服务价格

export function moneyEdit(data) {
	return request.post('/api/worker.service/money_edit', data);
}
//IM获取用户信息
export function getImUserInfo(data) {
	return request.get('/api/zhiqian.index/user_info', data);
}
//投诉类型
export function getComplaintList(data) {
	return request.get('/api/im.index/complaint_type_list', data);
}
//投诉
export function addComplaint(data) {
	return request.post('/api/im.index/complaint_add', data);
}
//IM获取手机号
export function getIMMobile() {
	return request.get('/api/im.index/get_mobile');
}
//修改手机号
export function updateMobile(data) {
	return request.post('/api/im.index/update_mobile', data);
}
//IM获取微信号
export function getIMWechat(data) {
	return request.get('/api/im.index/get_weixin', data);
}
//修改微信号
export function updateWechat(data) {
	return request.post('/api/im.index/update_weixin', data);
}
//发送联系方式
export function sendIMContact(data) {
	return request.post('/api/im.index/exchange', data);
}
//确认联系方式
export function exchangeContact(data) {
	return request.post('/api/im.index/exchange_confirm', data);
}
//添加需求
export function saveNeed(data) {
	return request.post('/api/boss.needs/add', data);
}
//编辑需求
export function editNeed(data) {
	return request.post('/api/boss.needs/edit', data);
}
//需求列表
export function needList(data) {
	return request.get('/api/boss.needs/needs_list', data);
}
//取消需求
export function needCancel(data) {
	return request.post('/api/boss.needs/stop', data);
}
//首页banner
export function getBannerlist() {
	return request.get('/api/boss.index/index');
}
//首页筛选条件
export function filterList() {
	return request.get('/api/boss.index/worker_type_filter');
}

//获取阿姨列表
export function getList(data) {
	return request.post('/api/boss.index/search', data);
}
//获取福利列表
export function welfareList() {
	return request.get('/api/dict.others/welfare');
}
//阿姨详情
export function getInfo(data) {
	return request.get('/api/worker.index/index', data);
}
//阿姨详情评价
export function getAyiComments(data) {
	return request.get('/api/worker.index/comment', data);
}
//阿姨服务价格
export function getAyiMoney(data) {
	return request.post('/api/worker.index/goods_list', data);
}
//关注列表
export function followList(data) {
	return request.post('/api/boss.follow/follow_list', data);
}
//关注阿姨
export function followAyi(data) {
	return request.post('/api/boss.follow/add', data);
}
//取消关注
export function cancelFollowAyi(data) {
	return request.post('/api/boss.follow/cancel', data);
}
//支付
export function payMent(data) {
	return request.post('/api/pay/pre_pay_order', data);
}
//签约下单
export function signOrderadd(data) {
	return request.post('/api/zhiqian.order/add', data);
}
//修改签约单
export function signOrderedit(data) {
	return request.post('/api/zhiqian.order/edit', data);
}
//签约单明细
export function getPriceInfo(data) {
	return request.get('/api/zhiqian.order/order_price_detail', data);
}
//订单列表
export function getOrderList(data) {
	return request.get('/api/zhiqian.order/boss_order_list', data);
}
//订单详情
export function getOrderInfo(data) {
	return request.get('/api/zhiqian.order/index', data);
}
//取消订单
export function cancelOrder(data) {
	return request.get('/api/zhiqian.order/cancel', data);
}
//确认订单
export function submitOrder(data) {
	return request.get('/api/zhiqian.order/boss_pay_off', data);
}
// 手续费与预定须知
export function getOrderPre() {
	return request.get('/api/zhiqian.order/pre');
}
//客服init
export function kefuInit() {
	return request.get('/addons/shopro/chat.index/init');
}
//发表评价
export function subComment(data) {
	return request.post('/api/notice.comment/boss_comment_add', data);
}
//评价列表
export function getCommentList(data) {
	return request.get('/api/notice.comment/boss_comment_list',data);
}
//删除评价
export function delComment(data) {
	return request.get('/api/notice.comment/boss_comment_cancel',data);
}
//保险列表
export function getBaoxian(data) {
	return request.get('/api/baoxian.index/order_list',data);
}
//分享参数
export function getShareCode() {
	return request.get('/api/share/relay');
}
//分享识别
export function shareAdd(data) {
	return request.post('/api/share/add', data);
}
//生成海报
export function getShareImg(data) {
	return request.get('/api/share/boss',data);
}
//实名下单
export function readNamePay() {
	return request.get('/api/real.index/real_order_add');
}
//实名支付
export function readPay(data) {
	return request.post('/api/pay/pre_pay_real',data);
}
//推荐
export function recAdd(data) {
	return request.post('/api/boss.rec/add',data);
}






